package br.com.builders.treinamento.service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.DuplicateEntryException;
import br.com.builders.treinamento.exception.ErrorCodes;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.ResourceNotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.utils.MessageUtil;

@Service
public class CustomerService {
	private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);
	 
	@Autowired
	private CustomerRepository repository;
	private MessageUtil apiMessage;
	
	@Autowired
	public CustomerService(CustomerRepository customerRepository, MessageUtil apiMessage) {
		this.repository = customerRepository;
		this.apiMessage = apiMessage;
	}
	
		
	public Customer createCustomer(Customer customer) throws DuplicateEntryException {
		
		logger.info("createCustomer - Creating customer " + customer.getName());
		if ( customer.getId() == null) {
			customer.setId(UUID.randomUUID().toString());
			logger.info("createCustomer - Generating UUID " + customer.getId());
		}
		if ( customer.getCrmId() == null ) {		
			customer.setCrmId(generateCrmId());
			logger.info("createCustomer - Generating cmmId " + customer.getCrmId());
		}	
		if ( repository.findOne(customer.getId()) != null) {			
			throw new DuplicateEntryException(apiMessage.getApiMessage(ErrorCodes.CUSTOMER_ALREADY_REGISTERED, customer.getId()));	
		}
		return repository.save(customer);
		 
	}	
	public Customer getCustomerById(String customerId) throws ResourceNotFoundException {	
		logger.info("getCustomerById - starting");
		Customer customer = repository.findCustomerById(customerId)
									.orElseThrow(() -> new ResourceNotFoundException(
									apiMessage.getApiMessage(ErrorCodes.CUSTOMER_NOT_REGISTERED, customerId)));	
		return customer;
	}
		
	public Customer getCustomerByCrmId(String crmId) throws NotFoundException {
		Customer customer = repository.findCustomerByCrmId(crmId);
		return customer;
	}
	public String generateCrmId() {
		Random random = new Random();
		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();	
		long number = random.nextInt(900000) + 100000;
		String crmId = String.valueOf(chars[random.nextInt(chars.length)]) + number;
		return crmId;		
	}
	public List<Customer> listCustomers() {
		logger.info("listCustomers - starting");	
		List<Customer> list = repository.findAll();
		return list;
	}
	
	public void deleteCustomer(String customerId) throws ResourceNotFoundException {
		logger.info("deleteCustomer - starting for id : " + customerId);	
		repository.findCustomerById(customerId)
				  .orElseThrow(() -> new ResourceNotFoundException(apiMessage.getApiMessage(ErrorCodes.CUSTOMER_NOT_REGISTERED,	customerId)));	
		repository.delete(customerId);
	}
	
	public Customer modifyCustomer(String customerId, java.util.Map<Object, Object> updates) throws ResourceNotFoundException {
		logger.info("modifyCustomer - starting");
		
		// Se objeto não existir, gera exception
		Customer customer = getCustomerById(customerId);
		
	    // Mapeando campos por reflection e atualizando o que estiver presente
		 updates.forEach((key, value) -> {
	       // use reflection to get field k on manager and set it to value k
	        Field field = ReflectionUtils.findField(Customer.class,(String) key);
	        field.setAccessible(true);
	        ReflectionUtils.setField(field, customer, value);
	    });
	    		
		return repository.save(customer);
	}

	public Customer replaceCustomer(String customerId, Customer customer) throws ResourceNotFoundException {

		logger.info("replaceCustomer - starting: customerId: " + customerId + " customerDto: " + customer.getId());	

		// Se objeto não existir, gera exception
		getCustomerById(customerId);
		
		logger.info("replaceCustomer - customers exists");	
		
		//tudo certo, salva
		return repository.save(customer);

	}		
}
