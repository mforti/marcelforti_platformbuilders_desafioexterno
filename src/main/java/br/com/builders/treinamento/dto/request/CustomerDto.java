/*
* Copyright 2018 Builders
*************************************************************
*Nome     : SampleRequest.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.dto.request;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.builders.treinamento.exception.ErrorCodes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "Customer")
public class CustomerDto {

	@ApiModelProperty(example = "553fa88c-4511-445c-b33a-ddff58d76886", value = "Internal Customer ID, uniquely identifying this customer in the world. ")
	// Sem consistencia @NotBlank pois se não informado no json, o uuid global esta sendo gerado no service
	private String id;

	// Sem consistencia @NotBlank pois se não informado no json, gera automaticamente no service conforme exemplo
	@ApiModelProperty(example = "C645235", value = "Customer ID in the CRM. ")
	private String crmId;

	@ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container. ")
	@URL(message = ErrorCodes.URL_INVALID_FORMAT)
	private String baseUrl;

	@ApiModelProperty(example = "Platform Builders", value = "Customer name ")
	@NotBlank(message = ErrorCodes.NAME_NOT_INFORMED)
	private String name;

	@ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login ")
	@NotBlank(message = ErrorCodes.LOGIN_NOT_INFORMED)
	@Email(message = ErrorCodes.LOGIN_INVALID_FORMAT)
	private String login;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
