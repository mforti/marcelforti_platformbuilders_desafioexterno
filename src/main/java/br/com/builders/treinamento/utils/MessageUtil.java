package br.com.builders.treinamento.utils;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class MessageUtil {

	private MessageSource source;

	@Autowired
	public MessageUtil(MessageSource source) {
		this.source = source;
	}

	public String getApiMessage(String messageCode, Object... args) {
		return source.getMessage(messageCode, args, Locale.getDefault());
	}
}
