/*
* Copyright 2018 Builders
*************************************************************
*Nome     : NotFoundException.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;

public class DuplicateEntryException extends APIException {

	private static final long serialVersionUID = -7353098254744436336L;

	public DuplicateEntryException(final Throwable cause) {
		super(HttpStatus.NOT_FOUND, cause);
	}

	public DuplicateEntryException(final String msg) {
		super(HttpStatus.NOT_FOUND, new Throwable(msg));
	}
	
	
}

