/*
* Copyright 2018 Builders
*************************************************************
*Nome     : ErrorCodes.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

public interface ErrorCodes {

  String REQUIRED_FIELD_NOT_INFORMED = "requiredField.not.informed";

  String VALIDATE_FIELD_MAX_LENGTH = "validateField.max.length";

  String VALIDATE_FIELD_MIN_LENGTH = "validateField.min.length";

  String VALIDATE_FIELD_INVALID_RANGE = "validateField.invalid.length";

  String VALIDATE_RECORD_ALREADY_REGISTERED = "validateRecord.already.registered";

  String VALIDATE_FIELD_INVALID_PATTERN = "validateField.invalid.pattern";
  
  String CUSTOMER_ALREADY_REGISTERED = "customer.already.registered";

  String CUSTOMER_NOT_REGISTERED = "customer.not.registered";
  
  String LOGIN_NOT_INFORMED = "login.not.informed";
  
  String NAME_NOT_INFORMED = "name.not.informed";
  
  String LOGIN_INVALID_FORMAT = "login.invalid.format";
  
  String URL_INVALID_FORMAT = "url.invalid.format";
  
  String MALFORMED_JSON = "malformed.json";
  
  String CUSTOMER_DELETED_SUCCESSFUL = "customer.deleted.successful";
  
  String CUSTOMER_CREATED_SUCCESSFUL = "customer.created.successful";
  
  String REPLACEMENT_NOT_POSSIBLE = "replacement.not.possible";
  
}
