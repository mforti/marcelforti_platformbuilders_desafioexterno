package br.com.builders.treinamento.resources;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.builders.treinamento.dto.request.CustomerDto;
import br.com.builders.treinamento.exception.DuplicateEntryException;
import br.com.builders.treinamento.exception.ErrorMessage;
import br.com.builders.treinamento.exception.ResourceNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "customers")
public interface TreinamentoResource {

	@ApiOperation(value = "Creates new customer", response = Void.class, tags = { "system", })
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Item created", response = Void.class),
	@ApiResponse(code = 400, message = "Invalid input, object invalid", response = Void.class),
	@ApiResponse(code = 409, message = "An existing item (login) already exists", response = Void.class) })
	ResponseEntity<Void> createCustomer(@ApiParam(value = "Customer to create") CustomerDto customerDto) throws DuplicateEntryException;

	@ApiOperation(value = "Deletes a customer", response = Void.class, tags = { "system", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item deleted", response = Void.class),
	@ApiResponse(code = 404, message = "The customer is not found", response = Void.class) })
	/*ResponseEntity<Void> deleteCustomer(
			@ApiParam(value = "ID of customer that needs to be deleted", required = true) String customerId)
			throws ResourceNotFoundException; */
	ResponseEntity<List<ErrorMessage>> deleteCustomer(
			@ApiParam(value = "ID of customer that needs to be deleted", required = true) String customerId)
			throws ResourceNotFoundException; 
	

	@ApiOperation(value = "List all data about a customer", response = CustomerDto.class, tags = { "customers", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "A single customer", response = CustomerDto.class),
	@ApiResponse(code = 404, message = "Customer with this ID not found", response = Void.class) })
	ResponseEntity<CustomerDto> getCustomer(
			@ApiParam(value = "ID of customer that needs to be fetched", required = true) String customerId)
			throws ResourceNotFoundException;

	@ApiOperation(value = "List all customers / containers (with essential data, enough for displaying a table)", response = CustomerDto.class, responseContainer = "List", tags = {
			"customers", })
	@ApiResponses(value = {
	@ApiResponse(code = 200, message = "Search results matching criteria", response = CustomerDto.class, responseContainer = "List") })
	ResponseEntity<List<CustomerDto>> listCustomers();

	@ApiOperation(value = "Modifies a customer", response = Void.class, tags = { "system", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item modified", response = Void.class),
	@ApiResponse(code = 400, message = "Invalid input, object invalid", response = Void.class),
	@ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found", response = Void.class) })	
	ResponseEntity<Void> modifyCustomer(@ApiParam(value = "ID of customer modified", required = true) String customerId,
			@ApiParam(value = "Customer data different") @RequestBody Map<Object, Object> updates) throws ResourceNotFoundException;
	

	@ApiOperation(value = "Replaces a customer", response = Void.class, tags = { "system", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item replaced", response = Void.class),
	@ApiResponse(code = 400, message = "Invalid input, object invalid", response = Void.class),
	@ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found", response = Void.class) })
	ResponseEntity<Void> replaceCustomer(@ApiParam(value = "ID of customer that needs to be replaced", required = true) String customerId,
										@ApiParam(value = "Customer to replace") CustomerDto customerDto) throws ResourceNotFoundException;

}