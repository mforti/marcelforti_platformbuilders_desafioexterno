/*
* Copyright 2018 Builders
*************************************************************
*Nome     : SampleRepository.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.repository;


import java.util.Optional;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.builders.treinamento.domain.Customer;

@Lazy
@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {
	 @Query("{crmId:'?0'}")
	 Customer findCustomerByCrmId(String crmId);
	 
	 Optional<Customer> findCustomerById(String customerId);
	 	
}
