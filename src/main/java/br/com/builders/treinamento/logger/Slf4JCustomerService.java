/*
* Copyright 2018 Builders
*************************************************************
*Nome     : Slf4Sample.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.logger;

import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;
import br.com.builders.treinamento.utils.MessageUtil;

public abstract class Slf4JCustomerService extends CustomerService {

  public Slf4JCustomerService(CustomerRepository customerRepository, MessageUtil apiMessage) {
		super(customerRepository, apiMessage);	
	}

  abstract protected void createCustomer();

}
