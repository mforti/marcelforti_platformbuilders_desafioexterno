package br.com.builders.treinamento.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.collect.Lists;

import br.com.builders.treinamento.resources.TreinamentoResource;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerDto;
import br.com.builders.treinamento.exception.DuplicateEntryException;
import br.com.builders.treinamento.exception.ErrorCodes;
import br.com.builders.treinamento.exception.ErrorMessage;
import br.com.builders.treinamento.exception.ResourceNotFoundException;
import br.com.builders.treinamento.service.CustomerService;
import br.com.builders.treinamento.utils.MessageUtil;

@RestController
@RequestMapping("/api/customers")
public class CustomerController implements TreinamentoResource {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	private CustomerService customerService;
	private ModelMapper mapper;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	public CustomerController(CustomerService customerService, ModelMapper mapper, MessageSource messageSource) {
		this.customerService = customerService;
		this.mapper = mapper;
		this.messageSource = messageSource;
	}
	

	@Override
	@PostMapping
	@ResponseBody
	public ResponseEntity<Void> createCustomer( @Valid @RequestBody CustomerDto customerDto) throws DuplicateEntryException {

		logger.info("createCustomer - Creating customer: " + customerDto.getName());
		Customer customer = customerService.createCustomer(mapper.map(customerDto, Customer.class));
		logger.info("createCustomer - Customer mapped: " + customer.getName());
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(customer.getId()).toUri();
		
		/* successfully created message */
		List<ErrorMessage> errors = Lists.newArrayList();	
	   	String message = new MessageUtil(this.messageSource).getApiMessage(ErrorCodes.CUSTOMER_CREATED_SUCCESSFUL, customer.getId());
	   	ErrorMessage error = new ErrorMessage(ErrorCodes.CUSTOMER_CREATED_SUCCESSFUL, message);
	   	errors.add(error);
	   	 
	   	
		return ResponseEntity.created(location).build();
	}

	@Override
	@DeleteMapping("/{customerId}")
	@ResponseBody
	public ResponseEntity<List<ErrorMessage>> deleteCustomer(@PathVariable("customerId") String customerId) throws ResourceNotFoundException {
		
		logger.info("deleteCustomer - Starting deletion for : " + customerId);
		customerService.deleteCustomer(customerId);
		
		/* successfully deleted message */
		List<ErrorMessage> errors = Lists.newArrayList();	
	   	String message = new MessageUtil(this.messageSource).getApiMessage(ErrorCodes.CUSTOMER_DELETED_SUCCESSFUL, customerId);
	   	ErrorMessage error = new ErrorMessage(ErrorCodes.CUSTOMER_DELETED_SUCCESSFUL, message);
	   	errors.add(error);
	   
		return new ResponseEntity<>(errors, HttpStatus.OK);   

	}
	@Override
	@GetMapping("/{customerId}")
	@ResponseBody
	public ResponseEntity<CustomerDto> getCustomer(@PathVariable("customerId") String customerId) throws ResourceNotFoundException {
		logger.info("getCustomer - Getting Customer " + customerId);
		
		Customer customer = customerService.getCustomerById(customerId);
		CustomerDto customerDto = null;
		if ( customer != null) {
			customerDto = mapper.map(customer, CustomerDto.class);
			logger.info("getCustomer - Found Customer and mapped" + customerDto.getId());
			return ResponseEntity.ok(customerDto);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	@Override
	@GetMapping
	@ResponseBody
	public ResponseEntity<List<CustomerDto>> listCustomers() {
		logger.info("listCustomers - starting");
		List<CustomerDto> listAllCustomers = new ArrayList<>();
		customerService.listCustomers().forEach(customer -> listAllCustomers.add(mapper.map(customer, CustomerDto.class)));
		return ResponseEntity.ok(listAllCustomers);
	}

	@Override
	@PatchMapping("/{customerId}")
	@ResponseBody
	public ResponseEntity<Void> modifyCustomer(@PathVariable("customerId") String customerId,
		@RequestBody Map<Object, Object> updates) throws ResourceNotFoundException {

		logger.info("modifyCustomer - starting");

		customerService.modifyCustomer(customerId, updates);
		logger.info("modifyCustomer - modified for "+ customerId);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand().toUri();
		return ResponseEntity.status(HttpStatus.OK).location(location).build();
	}
	
	@Override
	@PutMapping("/{customerId}")
	@ResponseBody
	// replaces all entity
	public ResponseEntity<Void> replaceCustomer(@PathVariable("customerId") String customerId,
			@Valid @RequestBody CustomerDto customerDto) throws ResourceNotFoundException {

		logger.info("replaceCustomer - starting: customerId: " + customerId + " customerDto: " + customerDto.getId());	
		customerService.replaceCustomer(customerId, mapper.map(customerDto, Customer.class));

		logger.info("replaceCustomer - replaced for "+ customerId);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand().toUri();

		return ResponseEntity.status(HttpStatus.OK).location(location).build();
	}
	
}
