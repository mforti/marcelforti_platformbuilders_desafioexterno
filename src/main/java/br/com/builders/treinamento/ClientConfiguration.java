/*
* Copyright 2018 Builders
*************************************************************
*Nome     : ClientConfiguration.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento;


import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ClientConfiguration {

	@Bean
	public ModelMapper defaultModelMapper() {
		return new ModelMapper();
	}
}
