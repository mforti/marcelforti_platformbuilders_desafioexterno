package br.com.builders.treinamento;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.ResourceNotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest{
	@Autowired
	protected CustomerRepository repository;
	
	@Autowired
	private CustomerService customerService;
	
	@Test
	public void testCreateCustomer() throws ResourceNotFoundException{	
		/* Criando alguns clientes */
		for ( int i = 0; i < 20; i ++) {
			Customer customer = createCustomer(UUID.randomUUID().toString(), customerService.generateCrmId());
			repository.save(customer);
			
			Customer saved = customerService.getCustomerById(customer.getId());
			assertTrue(customer.getId().equals(saved.getId()));
		}
					
	}
	@Test
	public void testListCustomers(){													
		List<Customer> customers = customerService.listCustomers();
		int total = customers.size();	
		assertTrue(total == customers.size());							
	}
	@Test
	public void testGetCustomer_Exists(){													
		List<Customer> customers = customerService.listCustomers();
		customers.forEach(singleCustomer->{
			try {
				assertNotNull(customerService.getCustomerById(singleCustomer.getId()));
			} catch (ResourceNotFoundException e) {		
				e.printStackTrace();
			}
		});
	}
	@Test
	public void testGetCustomer_NotFound(){													
		List<Customer> customers = customerService.listCustomers();
		customers.forEach(singleCustomer->{
			try {
				customerService.getCustomerById("1234");
			} catch (ResourceNotFoundException e) {		
				assertTrue(true);
			}
		});
	}
	@Test
	public void testModifyCustomer_fieldCrmId() throws ResourceNotFoundException{
		List<Customer> lista = customerService.listCustomers();
		/* Ira pegar um cliente aleatorio*/
		Collections.shuffle(lista);
		Customer customer = lista.get(0);
		System.out.println("ID Customer: " + customer.getId());
		
		/* novo valor*/
		customer.setCrmId("A123456");
		
		/* Mapeia classe */
		ObjectMapper oMapper = new ObjectMapper();
		Map<Object, Object> map = oMapper.convertValue(customer, Map.class);	
		Customer changed = customerService.modifyCustomer(customer.getId(), map );	
		System.out.println("ID Customer Changed: " + changed.getId());
		System.out.println("Customer changed: " + customerService.getCustomerById(changed.getId()).getCrmId());
		assertTrue(changed.getCrmId().equals("A123456"));		
	}
	@Test
	public void testModifyCustomer_fieldBaseUrl() throws ResourceNotFoundException{
		List<Customer> lista = customerService.listCustomers();
		/* Ira pegar um cliente aleatorio*/
		Collections.shuffle(lista);
		Customer customer = lista.get(0);
		System.out.println("ID Customer: " + customer.getId());
		
		/* novo valor*/
		customer.setBaseUrl("http://www.teste.com.br");
		
		/* Mapeia classe */
		ObjectMapper oMapper = new ObjectMapper();
		Map<Object, Object> map = oMapper.convertValue(customer, Map.class);	
		Customer changed = customerService.modifyCustomer(customer.getId(), map );	
		System.out.println("ID Customer Changed: " + changed.getId());
		System.out.println("Customer changed: " + customerService.getCustomerById(changed.getId()).getBaseUrl());
		assertTrue(changed.getBaseUrl().equals("http://www.teste.com.br"));		
	}
	@Test
	public void testModifyCustomer_fieldName() throws ResourceNotFoundException{
		List<Customer> lista = customerService.listCustomers();
		/* Ira pegar um cliente aleatorio*/
		Collections.shuffle(lista);
		Customer customer = lista.get(0);
		System.out.println("ID Customer: " + customer.getId());
		
		/* novo valor*/
		customer.setName("Marcel Angelo Forti");
		
		/* Mapeia classe */
		ObjectMapper oMapper = new ObjectMapper();
		Map<Object, Object> map = oMapper.convertValue(customer, Map.class);	
		Customer changed = customerService.modifyCustomer(customer.getId(), map );	
		System.out.println("ID Customer Changed: " + changed.getId());
		System.out.println("Customer changed: " + customerService.getCustomerById(changed.getId()).getName());
		assertTrue(changed.getName().equals("Marcel Angelo Forti"));		
	}
	@Test
	public void testModifyCustomer_fieldLogin() throws ResourceNotFoundException{
		List<Customer> lista = customerService.listCustomers();
		/* Ira pegar um cliente aleatorio*/
		Collections.shuffle(lista);
		Customer customer = lista.get(0);
		System.out.println("ID Customer: " + customer.getId());
		
		/* novo valor*/
		customer.setLogin("marcel.forti@gmail.com");
		
		/* Mapeia classe */
		ObjectMapper oMapper = new ObjectMapper();
		Map<Object, Object> map = oMapper.convertValue(customer, Map.class);	
		Customer changed = customerService.modifyCustomer(customer.getId(), map );	
		System.out.println("ID Customer Changed: " + changed.getId());
		System.out.println("Customer changed: " + customerService.getCustomerById(changed.getId()).getLogin()
				);
		assertTrue(changed.getLogin().equals("marcel.forti@gmail.com"));		
	}
	@Test
	public void testReplaceCustomer() throws ResourceNotFoundException{
		List<Customer> lista = customerService.listCustomers();
		/* Ira pegar um cliente aleatorio*/
		Collections.shuffle(lista);
		Customer customer = lista.get(0);
		System.out.println("ID Customer: " + customer.getId());
		
		Customer novo = new Customer();
		novo.setId(UUID.randomUUID().toString());
		novo.setBaseUrl("http://www.replacecustomer.com.br");
		novo.setCrmId("A123456");
		novo.setLogin("replaced@teste.com.br");
		novo.setName("Replaced Customer");
							
		customerService.replaceCustomer(customer.getId(), novo);
		
		
		Customer novo2 = customerService.getCustomerById(novo.getId());	
		assertTrue(novo2.getBaseUrl().equals(novo.getBaseUrl()));
		assertTrue(novo2.getCrmId().equals(novo.getCrmId()));
		assertTrue(novo2.getLogin().equals(novo.getLogin()));
		assertTrue(novo2.getName().equals(novo.getName()));
		try {
			customerService.getCustomerById(customer.getId());
		}catch (ResourceNotFoundException e) {
			assertTrue(true);
		}		
	}
	@Test
	public void testReplaceCustomer_notFound() throws ResourceNotFoundException{
				
		Customer novo = new Customer();
		novo.setId(UUID.randomUUID().toString());
		novo.setBaseUrl("http://www.replacecustomer.com.br");
		novo.setCrmId("A123456");
		novo.setLogin("replaced@teste.com.br");
		novo.setName("Replaced Customer");
		try {					
			customerService.replaceCustomer("123487", novo);
		}catch (ResourceNotFoundException e) {
			assertTrue(true);
		}				
	}
	//@Test
	public void testDeleteCustomer() throws ResourceNotFoundException{
		List<Customer> lista = customerService.listCustomers();
		/* Ira pegar um cliente aleatorio*/
		Collections.shuffle(lista);
		
		/* deleta  clientes existentes */
		for (Customer c : lista) {
			customerService.deleteCustomer(c.getId());
			try {
				customerService.getCustomerById(c.getId());
			}catch (ResourceNotFoundException e) {
				assertTrue(true);
			}
		}
		
	}
	@Test
	public void testDeleteCustomer_notFound() throws ResourceNotFoundException{
	
		try {
			customerService.deleteCustomer("NotExists");
		}catch (ResourceNotFoundException e) {
			assertTrue(true);
		}		
		
	}

	protected Customer createCustomer(String customerId, String crmId){
		List<Character> temp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".chars()
									.mapToObj(i -> (char)i)
									.collect(Collectors.toList());
	    Collections.shuffle(temp, new SecureRandom());
		String random =  temp.stream()
	            .map(Object::toString)
	            .limit(10)
	            .collect(Collectors.joining());
		
		Customer customer = new Customer();
		customer.setId(customerId);
		customer.setCrmId(crmId);
		customer.setBaseUrl("http://www.platformbuilders.com.br");
		customer.setName( "Desafio Platform Builders " + random );
		customer.setLogin("teste@teste.com.br");
		
		return customer;
		
	}
}
