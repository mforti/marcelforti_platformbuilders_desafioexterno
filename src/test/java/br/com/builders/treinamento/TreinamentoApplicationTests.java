package br.com.builders.treinamento;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.builders.treinamento.config.SimpleCORSFilter;
import br.com.builders.treinamento.controller.CustomerController;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import({ EmbeddedMongoAutoConfiguration.class })
public class TreinamentoApplicationTests {

	private MockMvc mvc;
	
	@MockBean
	private CustomerService customerServiceMock;
	
	@MockBean
	private CustomerController customerControllerMock;
	
	@Mock
	private CustomerRepository repository;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	private Customer customer = new Customer();

	@Before
	public void setup() {	
		mvc = MockMvcBuilders
                .standaloneSetup(customerControllerMock)
                .addFilters(new SimpleCORSFilter())
                .build();
		 
		/* Generate a customer */		
		customer.setId(UUID.randomUUID().toString());
		customer.setCrmId(customerServiceMock.generateCrmId());
		customer.setName("Test");
		customer.setBaseUrl("http://www.test.com.br");
		customer.setLogin("test@test.com.br");
	}

	@Test
	public void testCreateCustomer() throws Exception {
			
		when(customerServiceMock.getCustomerById(customer.getId())).thenReturn(null);
		when(customerServiceMock.createCustomer(customer)).thenReturn(customer);
		verify(repository).save(customer);
	}
	@Test
	public void testListCustomers() throws Exception {
		  assertThat(this.customerServiceMock).isNotNull();
	        mvc.perform(MockMvcRequestBuilders.get("/api/customers"))
	                .andExpect(status().isOk())
	                .andDo(print());
	}
	
	@Test
	public void testGetCustomerById_notFoud() throws Exception {
		String URL="/api/customers/123";
		
		this.mvc.perform(get(URL))	
			.andExpect(status().isNotFound());
	}

}